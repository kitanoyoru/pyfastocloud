About PyFastoCloud
===============

Python binding for FastoCloud services.

Dependencies
========
`setuptools` `pyfastocloud_base`

Install
========
`python3 setup.py install`
