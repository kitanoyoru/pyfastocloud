from pyfastocloud.client import Client, make_utc_timestamp_msec
from pyfastocloud.client_constants import ClientStatus, RequestReturn
from pyfastocloud.client_handler import IClientHandler
from pyfastocloud.structs.structs import OperationSystem


class Commands:
    # service
    ACTIVATE_COMMAND = "activate_request"
    STOP_SERVICE_COMMAND = "stop_service"
    SERVICE_PING_COMMAND = "ping_service"
    CLIENT_PING_COMMAND = "ping_client"  # ping from service
    CLIENT_SEND_MESSAGE = "send_message"
    GET_LOG_SERVICE_COMMAND = "get_log_service"
    SEND_SUBSCRIBER_NOTIFY = "send_message"
    SEND_SUBSCRIBER_SHUTDOWN = "send_shutdown"
    # broadcast
    STATISTIC_SERVICE_COMMAND = "statistic_service"
    SUBSCRIBER_CONNECTED = "subscriber_connected"
    SUBSCRIBER_DISCONNECTED = "subscriber_disconnected"
    CATCHUP_CREATED = "catchup_created"


class Fields:
    TIMESTAMP = "timestamp"
    LICENSE_KEY = "license_key"
    PATH = "path"
    DELAY = "delay"
    CATCHUPS_HOST = "catchups_host"
    CATCHUPS_HTTP_ROOT = "catchups_http_root"
    USER_ID = "id"
    DEVICE_ID = "device_id"


class FastoCloudLbClient(Client):
    PROJECT = "project"
    VERSION = "version"
    EXP_TIME = "expiration_time"
    OS = "os"

    def __init__(self, host: str, port: int, handler: IClientHandler, socket_mod):
        super().__init__(handler, socket_mod)
        self._host = host
        self._port = port
        self.__set_static_fields()

    @property
    def host(self) -> str:
        return self._host

    @property
    def port(self) -> int:
        return self._port

    @property
    def os(self) -> OperationSystem:
        return self._os

    @property
    def project(self) -> str:
        return self._project

    @property
    def version(self) -> str:
        return self._version

    @property
    def exp_time(self):
        return self._exp_time

    def connect(self) -> bool:
        if self.is_connected():
            return True

        sock = self.create_tcp_connection(self._host, self._port)
        if not sock:
            return False

        self._socket = sock
        self._set_state(ClientStatus.CONNECTED)
        return True

    def activate(self, command_id: int, license_key: str) -> RequestReturn:
        command_args = {Fields.LICENSE_KEY: license_key}
        return self._send_request(command_id, Commands.ACTIVATE_COMMAND, command_args)

    @Client.is_active_decorator
    def ping(self, command_id: int) -> RequestReturn:
        command_args = {Fields.TIMESTAMP: make_utc_timestamp_msec()}
        return self._send_request(
            command_id, Commands.SERVICE_PING_COMMAND, command_args
        )

    @Client.is_active_decorator
    def send_subscriber_notify(
        self, command_id: int, uid: str, device: str, params: dict
    ) -> RequestReturn:
        ser = {Fields.USER_ID: uid, Fields.DEVICE_ID: device}
        command_args = {**ser, **params}
        return self._send_request(
            command_id, Commands.SEND_SUBSCRIBER_NOTIFY, command_args
        )

    @Client.is_active_decorator
    def send_subscriber_shutdown(
        self, command_id: int, uid: str, device: str, params: dict
    ) -> RequestReturn:
        ser = {Fields.USER_ID: uid, Fields.DEVICE_ID: device}
        command_args = {**ser, **params}
        return self._send_request(
            command_id, Commands.SEND_SUBSCRIBER_SHUTDOWN, command_args
        )

    @Client.is_active_decorator
    def stop_service(self, command_id: int, delay: int) -> RequestReturn:
        command_args = {Fields.DELAY: delay}
        return self._send_request(
            command_id, Commands.STOP_SERVICE_COMMAND, command_args
        )

    @Client.is_active_decorator
    def get_log_service(self, command_id: int, path: str) -> RequestReturn:
        command_args = {Fields.PATH: path}
        return self._send_request(
            command_id, Commands.GET_LOG_SERVICE_COMMAND, command_args
        )

    def process_commands(self, data: bytes):
        if not data:
            return

        req, resp = self._decode_response_or_request(data)
        if req:
            if req.method == Commands.CLIENT_PING_COMMAND:
                self.__pong(req.id)

            if self._handler:
                self._handler.process_request(self, req)
        elif resp:
            saved_req, cb = self._pop_request(resp.id)
            if cb is not None:
                cb(saved_req, resp)

            if (
                saved_req
                and saved_req.method == Commands.ACTIVATE_COMMAND
                and resp.is_message()
            ):
                self._set_state(ClientStatus.ACTIVE)
                result = resp.result

                os = OperationSystem(**result[FastoCloudLbClient.OS])
                self.__set_static_fields(
                    result[FastoCloudLbClient.PROJECT],
                    result[FastoCloudLbClient.VERSION],
                    os,
                    result[FastoCloudLbClient.EXP_TIME],
                )
            elif (
                saved_req
                and saved_req.method == Commands.STOP_SERVICE_COMMAND
                and resp.is_message()
            ):
                self._reset()

            if self._handler:
                self._handler.process_response(self, saved_req, resp)

    # protected:
    def _reset(self):
        self.__set_static_fields()
        super()._reset()

    # private:
    @Client.is_active_decorator
    def __pong(self, command_id: str):
        ts = make_utc_timestamp_msec()
        self._send_response(command_id, {Fields.TIMESTAMP: ts})

    def __set_static_fields(self, project=None, version=None, os=None, exp_time=None):
        self._project = project
        self._version = version
        self._os = os
        self._exp_time = exp_time
