import json
import socket
import struct
from abc import ABC, abstractmethod
from datetime import datetime

from typing import Optional, Tuple

from pyfastocloud.client_constants import ClientStatus, RequestReturn
from pyfastocloud.client_handler import IClientHandler
from pyfastocloud.compressor_zlib import CompressorZlib
from pyfastocloud.json_rpc import (
    Request,
    Response,
    parse_response_or_request,
    JSON_RPC_OK_RESULT,
    JsonRPCErrorCode,
)


def make_utc_timestamp_seconds() -> int:
    return int(datetime.now().timestamp())


def make_utc_timestamp_msec() -> int:
    return make_utc_timestamp_seconds() * 1000


def generate_seq_id(command_id):  # uint64_t
    if command_id is None:
        return None

    converted_bytes = command_id.to_bytes(8, byteorder="big")
    return converted_bytes.hex()


RequestCallbackT = Tuple[Request, Response]


class Client(ABC):
    """
    Base client class for pyfastocloud connection
    """

    MAX_PACKET_SIZE = 64 * 1024 * 1024

    def is_active(self) -> bool:
        return self._state == ClientStatus.ACTIVE

    @staticmethod
    def is_active_decorator(function):
        """
        Decorator to check if client in active state (connected + license)
        """

        def closure(self, *args, **kwargs):
            if not self.is_active():
                return False, None
            return function(self, *args, *kwargs)

        return closure

    @property
    def status(self) -> ClientStatus:
        return self._state

    def is_connected(self) -> bool:
        return self._state != ClientStatus.INIT

    def disconnect(self):
        if not self.is_connected():
            return

        self._reset()

    @property
    def socket(self):
        return self._socket

    def read_command(self) -> Optional[bytearray]:
        if not self.is_connected():
            return None

        data_size_bytes = self._recv(4)
        if not data_size_bytes:
            return None

        data_size = struct.unpack(">I", data_size_bytes)[0]
        if data_size > Client.MAX_PACKET_SIZE:
            return None

        return self._recv(data_size)

    @abstractmethod
    def process_commands(self, data: bytes):
        pass

    def create_tcp_socket(self):
        return self._socket_mod.create_tcp_socket()

    def create_tcp_connection(self, host: str, port: int):
        try:
            sock = self.create_tcp_socket()
            sock.connect((host, port))
        except socket.error:
            return None

        return sock

    # protected
    def __init__(self, handler: IClientHandler, socket_mod):
        self._handler = handler
        self._gzip_compress = CompressorZlib(True)
        self._socket_mod = socket_mod

        self._socket = None
        self._state = ClientStatus.INIT
        self._request_queue = {}

    def _reset(self):
        if self._socket:
            self._socket.close()
            self._socket = None
        self._set_state(ClientStatus.INIT)
        self._request_queue = {}

    def _set_state(self, status: ClientStatus):
        self._state = status
        if self._handler:
            self._handler.on_client_state_changed(self, status)

    def _send_request(self, command_id, method: str, params) -> RequestReturn:
        return self._send_request_with_callback(command_id, method, params, None)

    def _send_request_with_callback(
        self, command_id: int, method: str, params, cb: Optional[RequestCallbackT]
    ) -> RequestReturn:
        if not self.is_connected():
            return False, None

        cid = generate_seq_id(command_id)
        req = Request(cid, method, params)

        data = json.dumps(req.to_dict())
        data_to_send_bytes = self._generate_data_to_send(data)

        try:
            self._socket.send(data_to_send_bytes)
        except socket.error:
            return False, None

        if not req.is_notification():
            self._request_queue[cid] = (req, cb)
        return True, cid

    def _pop_request(
        self, cid: str
    ) -> Tuple[Optional[Request], Optional[RequestCallbackT]]:
        saved_req = self._request_queue.pop(cid, None)
        if saved_req is not None:
            return saved_req
        return None, None

    def _generate_data_to_send(self, data: str) -> bytes:
        compressed = self._gzip_compress.compress(data.encode())
        compressed_len = len(compressed)
        array = struct.pack(">I", compressed_len)
        return array + compressed

    def _send_notification(self, method: str, params) -> RequestReturn:
        return self._send_request(None, method, params)

    def _send_response(self, command_id: str, params) -> bool:
        resp = Response.make_response_message(command_id, params)
        data = json.dumps(resp.to_dict())
        data_to_send_bytes = self._generate_data_to_send(data)
        try:
            self._socket.send(data_to_send_bytes)
        except socket.error:
            return False
        return True

    def _send_response_ok(self, command_id: str) -> bool:
        return self._send_response(command_id, JSON_RPC_OK_RESULT)

    def _send_response_fail(self, command_id: str, error: str) -> bool:
        resp = Response.make_response_error(
            command_id, error, JsonRPCErrorCode.JSON_RPC_SERVER_ERROR
        )
        data = json.dumps(resp.to_dict())
        data_to_send_bytes = self._generate_data_to_send(data)
        try:
            self._socket.send(data_to_send_bytes)
        except socket.error:
            return False
        return True

    def _recv(self, n: int) -> Optional[bytearray]:
        # Helper function to recv n bytes or return None if EOF is hit
        data = bytearray()
        while len(data) < n:
            try:
                packet = self._socket.recv(n - len(data))
            except socket.error:
                return None
            if not packet:
                return None
            data.extend(packet)
        return data

    def _decode_response_or_request(self, data: bytes) -> Tuple[Request, Response]:
        decoded_data = self._gzip_compress.decompress(data)
        return parse_response_or_request(decoded_data.decode())
