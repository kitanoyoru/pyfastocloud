from typing import List, Union
from enum import IntEnum

from pyfastogt.maker import Maker


class InferDataType(IntEnum):
    FLOAT = 0
    HALF = 1
    INT8 = 2
    INT32 = 3


class Rect:
    def __init__(self, x: int = 0, y: int = 0, width: int = 0, height: int = 0) -> None:
        self.x = x
        self.y = y
        self.width = width
        self.height = height


class InferLayer:
    NAME = "name"
    SIZE = "size"
    TYPE = "type"
    DATA = "data"

    def __init__(
        self, name: str, size: int, type: InferDataType, data: List[Union[int, float]]
    ) -> None:
        self.name = name
        self.size = size
        self.type = type
        self.data = data

    def to_dict(self) -> dict:
        d = dict()

        d[InferLayer.NAME] = self.name
        d[InferLayer.SIZE] = self.size
        d[InferLayer.TYPE] = self.type
        d[InferLayer.DATA] = self.data

        return d


class LabelInfo(Maker):
    LABEL = "label"
    LID = "id"
    CID = "class_id"
    PROBABILITY = "probability"

    def __init__(self) -> None:
        self.label = ""
        self.lid = 0
        self.cid = 0
        self.probability = 0.0

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, label = Maker.check_required_type(LabelInfo.LABEL, str, json)
        if res:
            self.label = label

        res, lid = Maker.check_required_type(LabelInfo.LID, int, json)
        if res:
            self.lid = lid

        res, cid = Maker.check_required_type(LabelInfo.CID, int, json)
        if res:
            self.cid = cid

        res, probability = Maker.check_required_type(LabelInfo.PROBABILITY, float, json)
        if res:
            self.probability = probability


class ImageBox(Maker):
    SENDER = "sender"
    TIMESTAMP = "timestamp"

    UNIQUE_COMPONENT_ID = "unique_component_id"
    CLASS_ID_FIELD = "class_id"
    OBJECT_ID_FIELD = "object_id"

    CONFIDENCE_FIELD = "confidence"
    # rect
    LEFT_FIELD = "left"
    TOP_FIELD = "top"
    WIDTH_FIELD = "width"
    HEIGHT_FIELD = "height"

    LAYERS = "layers"
    LABELS = "labels"

    def __init__(self):
        self.sender = ""
        self.timestamp = 0
        self.unique_component_id = 0
        self.class_id = 0
        self.object_id = 0
        self.confidence = 0.0
        self.rect = Rect()
        self.layers = []
        self.labels = []

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, sender = self.check_required_type(ImageBox.SENDER, str, json)
        if res:
            self.sender = sender

        res, timestamp = self.check_required_type(ImageBox.TIMESTAMP, int, json)
        if res:
            self.timestamp = timestamp

        res, unique_component_id = self.check_required_type(
            ImageBox.UNIQUE_COMPONENT_ID, int, json
        )
        if res:
            self.unique_component_id = unique_component_id

        res, class_id = self.check_required_type(ImageBox.CLASS_ID_FIELD, int, json)
        if res:
            self.class_id = class_id

        res, object_id = self.check_required_type(ImageBox.OBJECT_ID_FIELD, int, json)
        if res:
            self.object_id = object_id

        res, confidence = self.check_required_type(
            ImageBox.CONFIDENCE_FIELD, float, json
        )
        if res:
            self.confidence = confidence

        res, left = self.check_required_type(ImageBox.LEFT_FIELD, int, json)
        if res:
            self.rect.x = left

        res, top = self.check_required_type(ImageBox.TOP_FIELD, int, json)
        if res:
            self.rect.y = top

        res, width = self.check_required_type(ImageBox.WIDTH_FIELD, int, json)
        if res:
            self.rect.width = width

        res, height = self.check_required_type(ImageBox.HEIGHT_FIELD, int, json)
        if res:
            self.rect.height = height

        res, layers = self.check_required_type(ImageBox.LAYERS, list, json)
        if res:
            for layer in layers:
                self.layers.append(InferLayer(**layer))

        res, labels = self.check_required_type(ImageBox.LABELS, list, json)
        if res:
            for label in labels:
                label_info = LabelInfo()
                label_info.update_entry(label)
                self.labels.append(label_info)


class MlNotificationInfo(Maker):
    ID_FIELD = "id"
    IMAGES_FIELD = "images"

    def __init__(self):
        self._sid = None
        self._images = []

    def update_entry(self, json: dict):
        Maker.update_entry(self, json)

        res, sid = self.check_required_type(MlNotificationInfo.ID_FIELD, str, json)
        if res:
            self._sid = sid

        res, images = self.check_required_type(
            MlNotificationInfo.IMAGES_FIELD, list, json
        )
        if res:
            result = []
            for image in images:
                result.append(ImageBox.make_entry(image))

            self._images = result
