from abc import ABC, abstractmethod

from pyfastocloud.client_constants import ClientStatus


# handler for epg service client
class IStreamHandler(ABC):
    @abstractmethod
    def on_service_statistic_received(self, params: dict):
        pass

    @abstractmethod
    def on_client_state_changed(self, status: ClientStatus):
        pass

    @abstractmethod
    def on_ping_received(self, params: dict):
        pass
