from abc import ABC, abstractmethod

from pyfastocloud.client_constants import ClientStatus
from pyfastocloud.json_rpc import Response, Request


# handler for service client
class IStreamHandler(ABC):
    @abstractmethod
    def on_stream_statistic_received(self, params: dict):
        pass

    @abstractmethod
    def on_stream_sources_changed(self, params: dict):
        pass

    @abstractmethod
    def on_stream_ml_notification(self, params: dict):
        pass

    @abstractmethod
    def on_stream_result_ready(self, params: dict):
        pass

    @abstractmethod
    def on_webrtc_out_init_received(self, params: dict):
        pass

    @abstractmethod
    def on_webrtc_out_deinit_received(self, params: dict):
        pass

    @abstractmethod
    def on_webrtc_out_sdp_received(self, params: dict):
        pass

    @abstractmethod
    def on_webrtc_out_ice_received(self, params: dict):
        pass

    @abstractmethod
    def on_service_statistic_received(self, params: dict):
        pass

    @abstractmethod
    def on_quit_status_stream(self, params: dict):
        pass

    @abstractmethod
    def on_client_state_changed(self, status: ClientStatus):
        pass

    @abstractmethod
    def on_ping_received(self, params: dict):
        pass

    # rpc
    @abstractmethod
    def on_probe_in_stream(self, req: Request, rpc: Response):
        pass

    @abstractmethod
    def on_probe_out_stream(self, req: Request, rpc: Response):
        pass

    @abstractmethod
    def on_mounted_s3_bucket(self, req: Request, rpc: Response):
        pass

    @abstractmethod
    def on_unmounted_s3_bucket(self, req: Request, rpc: Response):
        pass

    @abstractmethod
    def on_list_s3_bucket(self, req: Request, rpc: Response):
        pass

    @abstractmethod
    def on_scan_folder(self, req: Request, rpc: Response):
        pass
