from pyfastocloud_base.streams.config import IStreamConfig

from pyfastocloud.client_constants import ClientStatus, RequestReturn
from pyfastocloud.client_handler import IClientHandler
from pyfastocloud.fastocloud_client import FastoCloudClient, Commands
from pyfastocloud.json_rpc import Request, Response
from pyfastocloud.node.service.stream_handler import IStreamHandler
from pyfastocloud.structs.structs import OperationSystem


class OnlineUsers(object):
    __slots__ = ["daemon", "http", "vods", "cods"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    @staticmethod
    def default() -> "OnlineUsers":
        result = OnlineUsers()
        result.daemon = 0
        result.http = 0
        result.vods = 0
        result.cods = 0
        return result

    def __str__(self):
        return "daemon:{0} http:{1} vods:{2} cods:{3}".format(
            self.daemon, self.http, self.vods, self.cods
        )

    def to_dict(self):
        return {
            "daemon": self.daemon,
            "http": self.http,
            "vods": self.vods,
            "cods": self.cods,
        }


class ServiceClient(IClientHandler):
    def __init__(self, host: str, port: int, handler: IStreamHandler, socket_mod):
        self._request_id = 0
        self._handler = handler
        self._client = FastoCloudClient(host, port, self, socket_mod)

    def connect(self) -> bool:
        return self._client.connect()

    def is_connected(self):
        return self._client.is_connected()

    @property
    def socket(self):
        return self._client.socket

    def recv_data(self) -> bool:
        data = self._client.read_command()
        if not data:
            return False

        self._client.process_commands(data)
        return True

    @property
    def status(self) -> ClientStatus:
        return self._client.status

    def probe_in_stream(self, url: dict) -> RequestReturn:
        return self._client.probe_in_stream(self.__gen_request_id(), url)

    def probe_out_stream(self, url: dict) -> RequestReturn:
        return self._client.probe_out_stream(self.__gen_request_id(), url)

    def mount_s3_bucket(
        self, name: str, path: str, key: str, secret: str
    ) -> RequestReturn:
        return self._client.mount_s3_bucket(
            self.__gen_request_id(), name, path, key, secret
        )

    def unmount_s3_bucket(self, path: str) -> RequestReturn:
        return self._client.unmount_s3_bucket(self.__gen_request_id(), path)

    def scan_folder(self, directory: str, extensions: list) -> RequestReturn:
        return self._client.scan_folder(self.__gen_request_id(), directory, extensions)

    def scan_s3_buckets(self) -> RequestReturn:
        return self._client.scan_s3_buckets(self.__gen_request_id())

    def disconnect(self):
        self._client.disconnect()

    def activate(self, license_key: str) -> RequestReturn:
        return self._client.activate(self.__gen_request_id(), license_key)

    def ping_service(self) -> RequestReturn:
        return self._client.ping(self.__gen_request_id())

    def stop_service(self, delay: int) -> RequestReturn:
        return self._client.stop_service(self.__gen_request_id(), delay)

    def get_log_service(self, route: str) -> RequestReturn:
        return self._client.get_log_service(self.__gen_request_id(), route)

    def start_stream(self, config: IStreamConfig) -> RequestReturn:
        return self._client.start_stream(self.__gen_request_id(), config)

    def stop_stream(self, stream_id: str, force: bool) -> RequestReturn:
        return self._client.stop_stream(self.__gen_request_id(), stream_id, force)

    def restart_stream(self, stream_id: str) -> RequestReturn:
        return self._client.restart_stream(self.__gen_request_id(), stream_id)

    def change_input_source_stream(
        self, stream_id: str, channel_id: int
    ) -> RequestReturn:
        return self._client.change_input_source_stream(
            self.__gen_request_id(), stream_id, channel_id
        )

    def webrtc_out_init_stream(
        self, stream_id: str, connection_id: str
    ) -> RequestReturn:
        return self._client.webrtc_out_init_stream(
            self.__gen_request_id(), stream_id, connection_id
        )

    def webrtc_out_deinit_stream(
        self, stream_id: str, connection_id: str
    ) -> RequestReturn:
        return self._client.webrtc_out_deinit_stream(
            self.__gen_request_id(), stream_id, connection_id
        )

    def webrtc_out_sdp_stream(
        self, stream_id: str, connection_id: str, sdp_description: str, sdp_type: str
    ) -> RequestReturn:
        return self._client.webrtc_out_sdp_stream(
            self.__gen_request_id(), stream_id, connection_id, sdp_description, sdp_type
        )

    def webrtc_out_ice_stream(
        self, stream_id: str, connection_id: str, candidate: str, mid: str, mlindex: int
    ) -> RequestReturn:
        return self._client.webrtc_out_ice_stream(
            self.__gen_request_id(), stream_id, connection_id, candidate, mid, mlindex
        )

    def inject_master_input_url(self, stream_id: str, url: dict) -> RequestReturn:
        return self._client.inject_master_input_url(
            self.__gen_request_id(), stream_id, url
        )

    def remove_master_input_url(self, stream_id: str, url: dict) -> RequestReturn:
        return self._client.remove_master_input_url(
            self.__gen_request_id(), stream_id, url
        )

    def get_log_stream(
        self, route: str, stream_id: str, feedback_directory: str
    ) -> RequestReturn:
        return self._client.get_log_stream(
            self.__gen_request_id(), stream_id, feedback_directory, route
        )

    def get_pipeline_stream(
        self, route: str, stream_id: str, feedback_directory: str
    ) -> RequestReturn:
        return self._client.get_pipeline_stream(
            self.__gen_request_id(), stream_id, feedback_directory, route
        )

    def get_config_json_stream(
        self, route: str, stream_id: str, feedback_directory: str
    ) -> RequestReturn:
        return self._client.get_config_json_stream(
            self.__gen_request_id(), stream_id, feedback_directory, route
        )

    @property
    def os(self) -> OperationSystem:
        return self._client.os

    @property
    def project(self) -> str:
        return self._client.project

    @property
    def version(self) -> str:
        return self._client.version

    @property
    def exp_time(self):
        return self._client.exp_time

    # handler
    def process_response(self, client, req: Request, resp: Response):
        if not req:
            return

        if req.method == Commands.ACTIVATE_COMMAND and resp.is_message():
            if self._handler:
                self._handler.on_service_statistic_received(resp.result)
        elif req.method == Commands.PROBE_IN_STREAM_COMMAND:
            if self._handler:
                self._handler.on_probe_in_stream(req, resp)
        elif req.method == Commands.PROBE_OUT_STREAM_COMMAND:
            if self._handler:
                self._handler.on_probe_out_stream(req, resp)
        elif req.method == Commands.PROBE_OUT_STREAM_COMMAND:
            if self._handler:
                self._handler.on_mounted_s3_bucket(req, resp)
        elif req.method == Commands.PROBE_OUT_STREAM_COMMAND:
            if self._handler:
                self._handler.on_unmounted_s3_bucket(req, resp)
        elif req.method == Commands.SCAN_S3_BUCKETS_COMMAND:
            if self._handler:
                self._handler.on_list_s3_bucket(req, resp)
        elif req.method == Commands.SCAN_FOLDER_COMMAND:
            if self._handler:
                self._handler.on_scan_folder(req, resp)

    def process_request(self, client, req: Request):
        if not req:
            return

        if not self._handler:
            return

        if req.method == Commands.STATISTIC_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_stream_statistic_received(req.params)
        elif req.method == Commands.CHANGED_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_stream_sources_changed(req.params)
        elif req.method == Commands.ML_NOTIFICATION_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_stream_ml_notification(req.params)
        elif req.method == Commands.RESULT_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_stream_result_ready(req.params)
        elif req.method == Commands.WEBRTC_OUT_INIT_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_webrtc_out_init_received(req.params)
        elif req.method == Commands.WEBRTC_OUT_DEINIT_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_webrtc_out_deinit_received(req.params)
        elif req.method == Commands.WEBRTC_OUT_SDP_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_webrtc_out_sdp_received(req.params)
        elif req.method == Commands.WEBRTC_OUT_ICE_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_webrtc_out_ice_received(req.params)
        elif req.method == Commands.STATISTIC_SERVICE_BROADCAST:
            assert req.is_notification()
            self._handler.on_service_statistic_received(req.params)
        elif req.method == Commands.QUIT_STATUS_STREAM_BROADCAST:
            assert req.is_notification()
            self._handler.on_quit_status_stream(req.params)
        elif req.method == Commands.CLIENT_PING_COMMAND:
            self._handler.on_ping_received(req.params)

    def on_client_state_changed(self, client, status: ClientStatus):
        if self._handler:
            self._handler.on_client_state_changed(status)

    # private
    def __gen_request_id(self) -> int:
        current_value = self._request_id
        self._request_id += 1
        return current_value
