from pyfastocloud.client_constants import ClientStatus
from pyfastocloud.client_handler import IClientHandler
from pyfastocloud.fastocloud_lb_client import (
    FastoCloudLbClient,
    Commands,
    RequestReturn,
)
from pyfastocloud.json_rpc import Request, Response
from pyfastocloud.node.load_balance.stream_handler import IStreamHandler
from pyfastocloud.structs.structs import OperationSystem


class OnlineUsers(object):
    __slots__ = ["daemon", "subscribers"]

    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            if key in self.__slots__:
                setattr(self, key, value)

    @staticmethod
    def default() -> "OnlineUsers":
        result = OnlineUsers()
        result.daemon = 0
        result.subscribers = 0
        return result

    def __str__(self):
        return "daemon:{0} subscribers:{1}".format(self.daemon, self.subscribers)

    def to_dict(self):
        return {"daemon": self.daemon, "subscribers": self.subscribers}


class ServiceClient(IClientHandler):
    def __init__(self, host: str, port: int, handler: IStreamHandler, socket_mod):
        self._request_id = 0
        self._online_subscribers = []
        self._handler = handler
        self._client = FastoCloudLbClient(host, port, self, socket_mod)
        self.__set_static_fields()

    def connect(self) -> bool:
        return self._client.connect()

    def is_connected(self):
        return self._client.is_connected()

    @property
    def socket(self):
        return self._client.socket

    def recv_data(self) -> bool:
        data = self._client.read_command()
        if not data:
            return False

        self._client.process_commands(data)
        return True

    @property
    def status(self) -> ClientStatus:
        return self._client.status

    def disconnect(self):
        self._client.disconnect()

    def activate(self, license_key: str) -> RequestReturn:
        return self._client.activate(self.__gen_request_id(), license_key)

    def send_subscriber_notify(self, uid: str, device: str, msg: dict) -> RequestReturn:
        return self._client.send_subscriber_notify(
            self.__gen_request_id(), uid, device, msg
        )

    def send_subscriber_shutdown(
        self, uid: str, device: str, msg: dict
    ) -> RequestReturn:
        return self._client.send_subscriber_shutdown(
            self.__gen_request_id(), uid, device, msg
        )

    def ping_service(self) -> RequestReturn:
        return self._client.ping(self.__gen_request_id())

    def stop_service(self, delay: int) -> RequestReturn:
        return self._client.stop_service(self.__gen_request_id(), delay)

    def get_log_service(self, route: str) -> RequestReturn:
        return self._client.get_log_service(self.__gen_request_id(), route)

    @property
    def os(self) -> OperationSystem:
        return self._client.os

    @property
    def project(self) -> str:
        return self._client.project

    @property
    def version(self) -> str:
        return self._client.version

    @property
    def online_subscribers(self) -> [dict]:
        return self._online_subscribers

    @property
    def exp_time(self):
        return self._client.exp_time

    # handler
    def process_response(self, client, req: Request, resp: Response):
        if not req:
            return

        if req.method == Commands.ACTIVATE_COMMAND and resp.is_message():
            if "online_clients" in resp.result:
                self._online_subscribers = resp.result["online_clients"]

            if self._handler:
                self._handler.on_service_statistic_received(resp.result)

    def process_request(self, client, req: Request):
        if not req:
            return

        if not self._handler:
            return

        if req.method == Commands.STATISTIC_SERVICE_COMMAND:
            assert req.is_notification()
            result = req.params
            self._handler.on_service_statistic_received(result)
        elif req.method == Commands.CLIENT_PING_COMMAND:
            self._handler.on_ping_received(req.params)
        elif req.method == Commands.CATCHUP_CREATED:
            self._handler.on_catchup_created(req.params)
        elif req.method == Commands.SUBSCRIBER_CONNECTED:
            subscriber = req.params
            if subscriber not in self._online_subscribers:
                self._online_subscribers.append(subscriber)
                self._handler.on_subscriber_connected(subscriber)
        elif req.method == Commands.SUBSCRIBER_DISCONNECTED:
            subscriber = req.params
            if subscriber in self._online_subscribers:
                self._online_subscribers.remove(subscriber)
                self._handler.on_subscriber_disconnected(subscriber)

    def on_client_state_changed(self, client, status: ClientStatus):
        if status != ClientStatus.ACTIVE:
            self.__set_static_fields()
        if self._handler:
            self._handler.on_client_state_changed(status)

    # private
    def __set_static_fields(self):
        self._online_subscribers = []

    def __gen_request_id(self) -> int:
        current_value = self._request_id
        self._request_id += 1
        return current_value
