from abc import ABC, abstractmethod

from pyfastocloud.client_constants import ClientStatus


# handler for lb service client
class IStreamHandler(ABC):
    @abstractmethod
    def on_service_statistic_received(self, params: dict):
        pass

    @abstractmethod
    def on_client_state_changed(self, status: ClientStatus):
        pass

    @abstractmethod
    def on_ping_received(self, params: dict):
        pass

    @abstractmethod
    def on_catchup_created(self, params: dict):
        pass

    @abstractmethod
    def on_subscriber_connected(self, params: dict):
        pass

    @abstractmethod
    def on_subscriber_disconnected(self, params: dict):
        pass
