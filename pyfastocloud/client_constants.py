from enum import IntEnum
from typing import Tuple, Optional

RequestReturn = Tuple[bool, Optional[str]]


class ClientStatus(IntEnum):
    INIT = 0
    CONNECTED = 1
    ACTIVE = 2
