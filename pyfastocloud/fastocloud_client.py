from typing import Optional

from pyfastocloud_base.streams.config import IStreamConfig
from pyfastocloud_base.constants import ServiceStatisticInfo

from pyfastocloud.client import Client, make_utc_timestamp_msec, RequestCallbackT
from pyfastocloud.client_constants import ClientStatus, RequestReturn
from pyfastocloud.client_handler import IClientHandler
from pyfastocloud.structs.structs import OperationSystem


class Commands:
    # stream
    START_STREAM_COMMAND = "start_stream"
    STOP_STREAM_COMMAND = "stop_stream"
    RESTART_STREAM_COMMAND = "restart_stream"
    CLEAN_STREAM_COMMAND = "clean_stream"
    GET_LOG_STREAM_COMMAND = "get_log_stream"
    GET_PIPELINE_STREAM_COMMAND = "get_pipeline_stream"
    GET_CONFIG_JSON_STREAM_COMMAND = "get_config_json_stream"
    CHANGE_INPUT_STREAM_COMMAND = "change_input_stream"

    WEBRTC_OUT_INIT_STREAM = "webrtc_out_init_stream"
    WEBRTC_OUT_SDP_STREAM = "webrtc_out_sdp_stream"
    WEBRTC_OUT_ICE_STREAM = "webrtc_out_ice_stream"
    WEBRTC_OUT_DEINIT_STREAM = "webrtc_out_deinit_stream"

    WEBRTC_IN_INIT_STREAM = "webrtc_in_init_stream"
    WEBRTC_IN_SDP_STREAM = "webrtc_in_sdp_stream"
    WEBRTC_IN_ICE_STREAM = "webrtc_in_ice_stream"
    WEBRTC_IN_DEINIT_STREAM = "webrtc_in_deinit_stream"

    INJECT_MASTER_INPUT_URL = "inject_master_input_url"
    REMOVE_MASTER_INPUT_URL = "remove_master_input_url"

    # broadcast
    CHANGED_STREAM_BROADCAST = "changed_source_stream"
    STATISTIC_STREAM_BROADCAST = "statistic_stream"
    ML_NOTIFICATION_STREAM_BROADCAST = "ml_notification_stream"
    RESULT_STREAM_BROADCAST = "result_stream"
    QUIT_STATUS_STREAM_BROADCAST = "quit_status_stream"

    WEBRTC_OUT_INIT_STREAM_BROADCAST = "webrtc_out_init_stream"
    WEBRTC_OUT_SDP_STREAM_BROADCAST = "webrtc_out_sdp_stream"
    WEBRTC_OUT_ICE_STREAM_BROADCAST = "webrtc_out_ice_stream"
    WEBRTC_OUT_DEINIT_STREAM_BROADCAST = "webrtc_out_deinit_stream"

    WEBRTC_IN_INIT_STREAM_BROADCAST = "webrtc_in_init_stream"
    WEBRTC_IN_SDP_STREAM_BROADCAST = "webrtc_in_sdp_stream"
    WEBRTC_IN_ICE_STREAM_BROADCAST = "webrtc_in_ice_stream"
    WEBRTC_IN_DEINIT_STREAM_BROADCAST = "webrtc_in_deinit_stream"

    # service
    ACTIVATE_COMMAND = "activate_request"
    STOP_SERVICE_COMMAND = "stop_service"
    SERVICE_PING_COMMAND = "ping_service"
    CLIENT_PING_COMMAND = "ping_client"  # ping from service
    GET_LOG_SERVICE_COMMAND = "get_log_service"
    MOUNT_S3_BUCKET = "mount_s3_bucket"
    UNMOUNT_S3_BUCKET = "unmount_s3_bucket"
    GET_HARDWARE_HASH_COMMAND = "get_hardware_key"
    # broadcast
    STATISTIC_SERVICE_BROADCAST = "statistic_service"

    PROBE_IN_STREAM_COMMAND = "probe_in_stream"
    PROBE_OUT_STREAM_COMMAND = "probe_out_stream"
    SCAN_FOLDER_COMMAND = "scan_folder"
    SCAN_S3_BUCKETS_COMMAND = "scan_s3_buckets"
    REMOVE_FOLDER_COMMAND = "remove_folder"


class Fields:
    TIMESTAMP = "timestamp"
    FEEDBACK_DIRECTORY = "feedback_directory"
    STREAMS = "streams"
    STREAM_ID = "id"
    FORCE = "force"
    ALGO = "algo"
    CHANNEL_ID = "channel_id"
    CONNECTION_ID = "connection_id"
    WEBRTC_DESCRIPTION = "description"
    WEBRTC_INIT = "init"
    WEBRTC_SDP = "sdp"
    WEBRTC_SDP_TYPE = "type"
    WEBRTC_ICE = "ice"
    WEBRTC_ICE_CANDIDATE = "candidate"
    WEBRTC_ICE_MLINDEX = "sdpMLineIndex"
    WEBRTC_ICE_MID = "sdpMid"
    WEBRTC_DEINIT = "deinit"
    LICENSE_KEY = "license_key"
    PATH = "path"
    KEY = "key"
    SECRET = "secret"
    URL = "url"
    CONFIG = "config"
    DELAY = "delay"
    DIRECTORY = "directory"
    EXTENSIONS = "extensions"
    DEFAULT_LOGO = "stream_logo_url"
    COMMAND = "command"
    PARAMS = "params"
    NAME = "name"


class FastoCloudClient(Client):
    PROJECT = "project"
    VERSION = "version"
    EXP_TIME = "expiration_time"
    OS = "os"

    def __init__(self, host: str, port: int, handler: IClientHandler, socket_mod):
        super().__init__(handler, socket_mod)
        self._host = host
        self._port = port
        self.__set_static_fields()

    @property
    def os(self) -> Optional[OperationSystem]:
        return self._os

    @property
    def project(self) -> Optional[str]:
        return self._project

    @property
    def version(self) -> Optional[str]:
        return self._version

    @property
    def exp_time(self):
        return self._exp_time

    @property
    def host(self) -> str:
        return self._host

    @property
    def port(self) -> int:
        return self._port

    def connect(self) -> bool:
        if self.is_connected():
            return True

        sock = self.create_tcp_connection(self._host, self._port)
        if not sock:
            return False

        self._socket = sock
        self._set_state(ClientStatus.CONNECTED)
        return True

    def activate(self, command_id: int, license_key: str) -> RequestReturn:
        return self.activate_with_callback(command_id, license_key, None)

    def activate_with_callback(
        self, command_id: int, license_key: str, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.LICENSE_KEY: license_key}
        return self._send_request_with_callback(
            command_id, Commands.ACTIVATE_COMMAND, command_args, callback
        )

    def ping(self, command_id: int) -> RequestReturn:
        return self.ping_with_callback(command_id, None)

    @Client.is_active_decorator
    def ping_with_callback(
        self, command_id: int, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.TIMESTAMP: make_utc_timestamp_msec()}
        return self._send_request_with_callback(
            command_id, Commands.SERVICE_PING_COMMAND, command_args, callback
        )

    def stop_service(self, command_id: int, delay: int) -> RequestReturn:
        return self.stop_service_with_callback(command_id, delay, None)

    def get_hardware_hash(
        self, command_id: int, algo: int, callback: RequestCallbackT
    ) -> RequestReturn:  # REFACTOR: In future create Algo struct in pyfastogt
        return self.get_hardware_hash_with_callback(command_id, algo, callback)

    @Client.is_active_decorator
    def get_hardware_hash_with_callback(
        self, command_id: int, algo: int, callback: RequestCallbackT
    ) -> RequestReturn:
        commands_args = {Fields.ALGO: algo}
        return self._send_request_with_callback(
            command_id, Commands.GET_HARDWARE_HASH_COMMAND, commands_args, callback
        )

    @Client.is_active_decorator
    def stop_service_with_callback(
        self, command_id: int, delay: int, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.DELAY: delay}
        return self._send_request_with_callback(
            command_id, Commands.STOP_SERVICE_COMMAND, command_args, callback
        )

    def get_log_service(self, command_id: int, path: str) -> RequestReturn:
        return self.get_log_service_with_callback(command_id, path, None)

    @Client.is_active_decorator
    def get_log_service_with_callback(
        self, command_id: int, path: str, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.PATH: path}
        return self._send_request_with_callback(
            command_id, Commands.GET_LOG_SERVICE_COMMAND, command_args, callback
        )

    def probe_in_stream(self, command_id: int, url: dict) -> RequestReturn:
        return self.probe_in_stream_with_callback(command_id, url, None)

    @Client.is_active_decorator
    def probe_in_stream_with_callback(
        self, command_id: int, url: dict, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.URL: url}
        return self._send_request_with_callback(
            command_id, Commands.PROBE_IN_STREAM_COMMAND, command_args, callback
        )

    def probe_out_stream(self, command_id: int, url: dict) -> RequestReturn:
        return self.probe_out_stream_with_callback(command_id, url, None)

    @Client.is_active_decorator
    def probe_out_stream_with_callback(
        self, command_id: int, url: dict, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.URL: url}
        return self._send_request_with_callback(
            command_id, Commands.PROBE_OUT_STREAM_COMMAND, command_args, callback
        )

    def mount_s3_bucket(
        self, command_id: int, name: str, path: str, key: str, secret: str
    ) -> RequestReturn:
        return self.mount_s3_bucket_with_callback(
            command_id, name, path, key, secret, None
        )

    @Client.is_active_decorator
    def mount_s3_bucket_with_callback(
        self,
        command_id: int,
        name: str,
        path: str,
        key: str,
        secret: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {
            Fields.NAME: name,
            Fields.PATH: path,
            Fields.KEY: key,
            Fields.SECRET: secret,
        }
        return self._send_request_with_callback(
            command_id, Commands.MOUNT_S3_BUCKET, command_args, callback
        )

    def unmount_s3_bucket(self, command_id: int, path: str) -> RequestReturn:
        return self.unmount_s3_bucket_with_callback(command_id, path, None)

    @Client.is_active_decorator
    def unmount_s3_bucket_with_callback(
        self, command_id: int, path: str, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.PATH: path}
        return self._send_request_with_callback(
            command_id, Commands.UNMOUNT_S3_BUCKET, command_args, callback
        )

    def scan_folder(
        self, command_id: int, directory: str, extensions: list
    ) -> RequestReturn:
        return self.scan_folder_with_callback(command_id, directory, extensions, None)

    @Client.is_active_decorator
    def scan_folder_with_callback(
        self,
        command_id: int,
        directory: str,
        extensions: list,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {Fields.DIRECTORY: directory, Fields.EXTENSIONS: extensions}
        return self._send_request_with_callback(
            command_id, Commands.SCAN_FOLDER_COMMAND, command_args, callback
        )

    def scan_s3_buckets(self, command_id: int) -> RequestReturn:
        return self.scan_s3_buckets_with_callback(command_id, None)

    @Client.is_active_decorator
    def scan_s3_buckets_with_callback(
        self, command_id: int, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {}
        return self._send_request_with_callback(
            command_id, Commands.SCAN_S3_BUCKETS_COMMAND, command_args, callback
        )

    @Client.is_active_decorator
    def remove_folder_with_callback(
        self, command_id: int, directory: str, callback: RequestCallbackT
    ) -> RequestReturn:
        command_args = {Fields.DIRECTORY: directory}
        return self._send_request_with_callback(
            command_id, Commands.REMOVE_FOLDER_COMMAND, command_args, callback
        )

    def start_stream(self, command_id: int, config: IStreamConfig) -> RequestReturn:
        return self.start_stream_with_callback(command_id, config, None)

    @Client.is_active_decorator
    def start_stream_with_callback(
        self,
        command_id: int,
        config: IStreamConfig,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {Fields.CONFIG: config.to_dict()}
        return self._send_request_with_callback(
            command_id, Commands.START_STREAM_COMMAND, command_args, callback
        )

    def clean_stream(self, command_id: int, config: IStreamConfig) -> RequestReturn:
        return self.clean_stream_with_callback(command_id, config, None)

    @Client.is_active_decorator
    def clean_stream_with_callback(
        self,
        command_id: int,
        config: IStreamConfig,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {Fields.CONFIG: config.to_dict()}
        return self._send_request_with_callback(
            command_id, Commands.CLEAN_STREAM_COMMAND, command_args, callback
        )

    def stop_stream(
        self, command_id: int, stream_id: str, force: bool
    ) -> RequestReturn:
        return self.stop_stream_with_callback(command_id, stream_id, force, None)

    @Client.is_active_decorator
    def stop_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        force: bool,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {Fields.STREAM_ID: stream_id, Fields.FORCE: force}
        return self._send_request_with_callback(
            command_id, Commands.STOP_STREAM_COMMAND, command_args, callback
        )

    def restart_stream(self, command_id: int, stream_id: str) -> RequestReturn:
        return self.restart_stream_with_callback(command_id, stream_id, None)

    @Client.is_active_decorator
    def restart_stream_with_callback(
        self, command_id: int, stream_id: str, callback: Optional[RequestCallbackT]
    ) -> RequestReturn:
        command_args = {Fields.STREAM_ID: stream_id}
        return self._send_request_with_callback(
            command_id, Commands.RESTART_STREAM_COMMAND, command_args, callback
        )

    def change_input_source_stream(
        self, command_id: int, stream_id: str, channel_id: int
    ) -> RequestReturn:
        return self.change_input_source_stream_with_callback(
            command_id, stream_id, channel_id, None
        )

    @Client.is_active_decorator
    def change_input_source_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        channel_id: int,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {Fields.STREAM_ID: stream_id, Fields.CHANNEL_ID: channel_id}
        return self._send_request_with_callback(
            command_id, Commands.CHANGE_INPUT_STREAM_COMMAND, command_args, callback
        )

    def webrtc_out_init_stream(
        self, command_id: int, stream_id: str, connection_id: str
    ) -> RequestReturn:
        return self.webrtc_out_init_stream_with_callback(
            command_id, stream_id, connection_id, None
        )

    @Client.is_active_decorator
    def webrtc_out_init_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        init = {Fields.CONNECTION_ID: connection_id}
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_INIT: init}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_OUT_INIT_STREAM, command_args, callback
        )

    def webrtc_out_deinit_stream(
        self, command_id: int, stream_id: str, connection_id: str
    ) -> RequestReturn:
        return self.webrtc_out_deinit_stream_with_callback(
            command_id, stream_id, connection_id, None
        )

    @Client.is_active_decorator
    def webrtc_out_deinit_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        init = {Fields.CONNECTION_ID: connection_id}
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_DEINIT: init}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_OUT_DEINIT_STREAM, command_args, callback
        )

    def webrtc_out_sdp_stream(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        sdp_description: str,
        sdp_type: str,
    ) -> RequestReturn:
        return self.webrtc_out_sdp_stream_with_callback(
            command_id, stream_id, connection_id, sdp_description, sdp_type, None
        )

    @Client.is_active_decorator
    def webrtc_out_sdp_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        sdp_description: str,
        sdp_type: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        sdp = {
            Fields.CONNECTION_ID: connection_id,
            Fields.WEBRTC_SDP: sdp_description,
            Fields.WEBRTC_SDP_TYPE: sdp_type,
        }
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_DESCRIPTION: sdp}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_OUT_SDP_STREAM, command_args, callback
        )

    def webrtc_out_ice_stream(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        candidate: str,
        mid: str,
        mlindex: int,
    ) -> RequestReturn:
        return self.webrtc_out_ice_stream_with_callback(
            command_id, stream_id, connection_id, candidate, mid, mlindex, None
        )

    @Client.is_active_decorator
    def webrtc_out_ice_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        candidate: str,
        mid: str,
        mlindex: int,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        ice = {
            Fields.CONNECTION_ID: connection_id,
            Fields.WEBRTC_ICE_CANDIDATE: candidate,
            Fields.WEBRTC_ICE_MID: mid,
            Fields.WEBRTC_ICE_MLINDEX: mlindex,
        }
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_ICE: ice}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_OUT_ICE_STREAM, command_args, callback
        )

    def webrtc_in_init_stream(
        self, command_id: int, stream_id: str, connection_id: str
    ) -> RequestReturn:
        return self.webrtc_in_init_stream_with_callback(
            command_id, stream_id, connection_id, None
        )

    @Client.is_active_decorator
    def webrtc_in_init_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        init = {Fields.CONNECTION_ID: connection_id}
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_INIT: init}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_IN_INIT_STREAM, command_args, callback
        )

    def webrtc_in_deinit_stream(
        self, command_id: int, stream_id: str, connection_id: str
    ) -> RequestReturn:
        return self.webrtc_in_deinit_stream_with_callback(
            command_id, stream_id, connection_id, None
        )

    @Client.is_active_decorator
    def webrtc_in_deinit_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        init = {Fields.CONNECTION_ID: connection_id}
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_DEINIT: init}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_IN_DEINIT_STREAM, command_args, callback
        )

    def webrtc_in_sdp_stream(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        sdp_description: str,
        sdp_type: str,
    ) -> RequestReturn:
        return self.webrtc_in_sdp_stream_with_callback(
            command_id, stream_id, connection_id, sdp_description, sdp_type, None
        )

    @Client.is_active_decorator
    def webrtc_in_sdp_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        sdp_description: str,
        sdp_type: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        sdp = {
            Fields.CONNECTION_ID: connection_id,
            Fields.WEBRTC_SDP: sdp_description,
            Fields.WEBRTC_SDP_TYPE: sdp_type,
        }
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_DESCRIPTION: sdp}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_IN_SDP_STREAM, command_args, callback
        )

    def webrtc_in_ice_stream(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        candidate: str,
        mid: str,
        mlindex: int,
    ) -> RequestReturn:
        return self.webrtc_in_ice_stream_with_callback(
            command_id, stream_id, connection_id, candidate, mid, mlindex, None
        )

    @Client.is_active_decorator
    def webrtc_in_ice_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        connection_id: str,
        candidate: str,
        mid: str,
        mlindex: int,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        ice = {
            Fields.CONNECTION_ID: connection_id,
            Fields.WEBRTC_ICE_CANDIDATE: candidate,
            Fields.WEBRTC_ICE_MID: mid,
            Fields.WEBRTC_ICE_MLINDEX: mlindex,
        }
        command_args = {Fields.STREAM_ID: stream_id, Fields.WEBRTC_ICE: ice}
        return self._send_request_with_callback(
            command_id, Commands.WEBRTC_IN_ICE_STREAM, command_args, callback
        )

    def inject_master_input_url(
        self, command_id: int, stream_id: str, url: dict
    ) -> RequestReturn:
        return self.inject_master_input_url_with_callback(
            command_id, stream_id, url, None
        )

    @Client.is_active_decorator
    def inject_master_input_url_with_callback(
        self,
        command_id: int,
        stream_id: str,
        url: dict,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {Fields.STREAM_ID: stream_id, Fields.URL: url}
        return self._send_request_with_callback(
            command_id, Commands.INJECT_MASTER_INPUT_URL, command_args, callback
        )

    def remove_master_input_url(
        self, command_id: int, stream_id: str, url: dict
    ) -> RequestReturn:
        return self.remove_master_input_url_with_callback(
            command_id, stream_id, url, None
        )

    @Client.is_active_decorator
    def remove_master_input_url_with_callback(
        self,
        command_id: int,
        stream_id: str,
        url: dict,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {Fields.STREAM_ID: stream_id, Fields.URL: url}
        return self._send_request_with_callback(
            command_id, Commands.REMOVE_MASTER_INPUT_URL, command_args, callback
        )

    def get_log_stream(
        self, command_id: int, stream_id: str, feedback_directory: str, path: str
    ) -> RequestReturn:
        return self.get_log_stream_with_callback(
            command_id, stream_id, feedback_directory, path, None
        )

    @Client.is_active_decorator
    def get_log_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        feedback_directory: str,
        path: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {
            Fields.STREAM_ID: stream_id,
            Fields.FEEDBACK_DIRECTORY: feedback_directory,
            Fields.PATH: path,
        }
        return self._send_request_with_callback(
            command_id, Commands.GET_LOG_STREAM_COMMAND, command_args, callback
        )

    def get_pipeline_stream(
        self, command_id: int, stream_id: str, feedback_directory: str, path: str
    ) -> RequestReturn:
        return self.get_pipeline_stream_with_callback(
            command_id, stream_id, feedback_directory, path, None
        )

    @Client.is_active_decorator
    def get_pipeline_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        feedback_directory: str,
        path: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {
            Fields.STREAM_ID: stream_id,
            Fields.FEEDBACK_DIRECTORY: feedback_directory,
            Fields.PATH: path,
        }
        return self._send_request_with_callback(
            command_id, Commands.GET_PIPELINE_STREAM_COMMAND, command_args, callback
        )

    def get_config_json_stream(
        self, command_id: int, stream_id: str, feedback_directory: str, path: str
    ) -> RequestReturn:
        return self.get_config_json_stream_with_callback(
            command_id, stream_id, feedback_directory, path, None
        )

    @Client.is_active_decorator
    def get_config_json_stream_with_callback(
        self,
        command_id: int,
        stream_id: str,
        feedback_directory: str,
        path: str,
        callback: Optional[RequestCallbackT],
    ) -> RequestReturn:
        command_args = {
            Fields.STREAM_ID: stream_id,
            Fields.FEEDBACK_DIRECTORY: feedback_directory,
            Fields.PATH: path,
        }
        return self._send_request_with_callback(
            command_id, Commands.GET_CONFIG_JSON_STREAM_COMMAND, command_args, callback
        )

    def process_commands(self, data: bytes):
        if not data:
            return

        req, resp = self._decode_response_or_request(data)
        if req:
            if req.method == Commands.CLIENT_PING_COMMAND:
                self.__pong(req.id)
            elif req.method == Commands.STATISTIC_SERVICE_BROADCAST:
                stats = ServiceStatisticInfo(**req.params)
                self.stats = stats

            if self._handler:
                self._handler.process_request(self, req)

        elif resp:
            saved_req, cb = self._pop_request(resp.id)
            if cb is not None:
                cb(saved_req, resp)

            if saved_req:
                if saved_req.method == Commands.ACTIVATE_COMMAND and resp.is_message():
                    self._set_state(ClientStatus.ACTIVE)
                    result = resp.result

                    os = OperationSystem(**result[FastoCloudClient.OS])
                    self.__set_static_fields(
                        result[FastoCloudClient.PROJECT],
                        result[FastoCloudClient.VERSION],
                        os,
                        result[FastoCloudClient.EXP_TIME],
                    )
                elif (
                    saved_req.method == Commands.STOP_SERVICE_COMMAND
                    and resp.is_message()
                ):
                    self._reset()

            if self._handler:
                self._handler.process_response(self, saved_req, resp)

    # protected:
    def _reset(self):
        self.__set_static_fields()
        super()._reset()

    # private:
    @Client.is_active_decorator
    def __pong(self, command_id: str):
        ts = make_utc_timestamp_msec()
        self._send_response(command_id, {Fields.TIMESTAMP: ts})

    def __set_static_fields(
        self, project=None, version=None, os=None, exp_time=None, stats=None
    ):
        self._project = project
        self._version = version
        self._os = os
        self._stats = stats
        self._exp_time = exp_time
