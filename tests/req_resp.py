#!/usr/bin/env python3
import unittest

from pyfastocloud.json_rpc import Request, Response


class RequestResponseTest(unittest.TestCase):
    def test_request(self):
        params = {"hello": 1}
        command = "some"
        cid = "123"
        req = Request(cid, command, params)
        self.assertTrue(req.is_valid())
        self.assertFalse(req.is_notification())
        self.assertEqual(cid, req.id)
        self.assertEqual(command, req.method)
        self.assertEqual(params, req.params)

    def test_broadcast(self):
        params = {"hello": 1}
        command = "broadcast"
        cid = None
        req = Request(cid, command, params)
        self.assertTrue(req.is_valid())
        self.assertTrue(req.is_notification())
        self.assertEqual(cid, req.id)
        self.assertEqual(command, req.method)
        self.assertEqual(params, req.params)

    def test_invalid_request(self):
        params = {"hello": 1}
        command = ""
        cid = "123"
        req = Request(cid, command, params)
        self.assertFalse(req.is_valid())

    def test_response(self):
        result = {}
        cid = "123"
        res = Response(cid, result)
        self.assertEqual(cid, res.id)
        self.assertTrue(res.is_valid())
        self.assertTrue(res.is_message())
        self.assertEqual(result, res.result)


if __name__ == "__main__":
    unittest.main()
