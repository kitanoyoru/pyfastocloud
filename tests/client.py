#!/usr/bin/env python3
import unittest

from pyfastocloud.fastocloud_client import FastoCloudClient
from pyfastocloud.json_rpc import Request, Response
from pyfastocloud.socket import std


class ClientTest(unittest.TestCase):
    def test_lifetime(self):
        host = "fastocloud.com"
        port = 6317
        client = FastoCloudClient(host, port, None, std)
        self.assertEqual(client.host, host)
        self.assertEqual(client.port, port)
        self.assertFalse(client.is_active())

        res = client.connect()
        self.assertTrue(res)

        def active_cb(req: Request, resp: Response):
            self.assertTrue(req)
            self.assertTrue(resp)
            pass

        res, oid = client.activate_with_callback(
            0,
            "006962a0de000054b426e80000ad54e62c0170465e12fa0d50951740f10080eaaa2df109404aec98cb5c3e5990a8d1480",
            active_cb,
        )
        self.assertTrue(res)
        self.assertEqual(oid, "0000000000000000")

        data = client.read_command()
        client.process_commands(data)


if __name__ == "__main__":
    unittest.main()
